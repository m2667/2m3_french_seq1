# Notes sur les poemes de la Resistance

## Correction questions

    2.
    > Denonciation
    > Appellent a la Resisitance
    > Auteurs doivent echapper a la censure
    > Publies par les Editions de Minuit clandestines

    3. "Courage" et "Ce coeur qui haissait la guerre" sont des appels a la liberte. On notera les addresses et l'utilisation de l'imperatif. Eluard force les parisiens a faire face a l'Occupation et les exhorte a la perseverance et a la bravoure.

    4. Dans "Le veilleur du Pont-au-Charge", Desnos enumere les formes de resistance, propagande a l'aide de tracts, renseignement, saboteurs...
    > Dans le fragment 128 des feuillets d'Hypnos, Rene Char decrit un moment fort de sa vie de resistant. On releve les verbes d'action, les indices temporels et le vocabulaire de l'activite de la resistance. Sa vie quotidienne est dominee par l'incertitude et la peur.

    5. Les poetes utilisent la 1e personne, le present d'enonciation et les presentatifs.

    6. Montrent la force du groupe chez les resistants: l'individu est soutenu par le groupe. Utilisation d'enumeration et d'Hyperbole marquant leur nombre. Les poetes sihgnalent aussi que la Resistance depasse les frontieres.
