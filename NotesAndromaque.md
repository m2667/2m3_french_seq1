# Notes sur *Andromaque*

## Generalites

|> Lieu: a la cour de Pyrrhus, Epire

|> Temps: Debut de journee (?)

|> Passion: tres presente dans la tragedie racinienne

|> Chaque personnage aime quelqu'un mais n'est pas aime en retour (Oreste, Pyrrhus & Hermione)

|> Destin: Element essentiel de la tragedie (sort, fatalite)

|> Guerre/Politique: Pyrrhus refuse d'epouser Hermione, Pyrrhus semble proteger le fils d'Andromaque & d'Hector qui represente une menace pour les Grecs => menace d'une guerre

|> Tout se passe en une journee => Tensions au maximum

|> A4S1=> Andromaque accepte de marier Pyrrhus mais planne de se suicuider just apres

## A1S4

|> Pyrrhus veut defaire le mal de la guerre de Troie

|> Utilisation du present & futur  => pret a aider Andromaque des maintenant

|> Pyrrhus considere les Grecs comme ennemis

|> Est pret a donner sa couronne a Astyanax par amour pour Andromaque

|> Pyrrhus se paint comme une victime, supplice de l'amour => Compare au massacre de Troie

|> Andromaque inflechie, essaie de faire raisonner Pyrrhus

|> Pyrrhus se tourne vers les menaces oar desespoire

|> Andromaque prete a mourir si son fils meurt

## A4S5 (derniere tirade d'Hermione)

|> Questions rhetoriques

|> Champ lexical de l'amour et de la cruaute => Remords(?) d'Hermione

|> Personnification "bouche cruelle" => cruaute, voire inhumanite percue de Pyrrhus

|> "Vient [...] m'annoncer le trepas" => Hermione est prete a mourir/se sent mourante

|> "Achevez votre hymen, j'y consens" => resignee, n'a plus d'espoir

|> Va se suicider/tuer Pyrrhus: "Pour la derniere fois je vous parle"

|> Le menace de la colere des dieux

    |> Comment evolue la colere d'Hermione face a Pyrrhus?
        |- Essai de le faire raisonner
            |- Elle l'aime et fait tout pour que ce soit reciproque
            |- Pyrrhus ne peut pas marier Andromaque sans debuter une guerre
        |- Resignation
            |- Consent au mariage de Pyrrhus et Andromaque
            |- Elle doute encore si elle n'aime pas Pyrrhus => c'est contre sa volonte
        |- Menace
            |- La colere des dieux
            |- "Mais crains encor d'y trouver Hermione"
