# Notes sur les analyses de fables

## *Les deux Coqs*

|> Fable inspiree de celle d'Esope

|> Commence avec mention de Troie --> Parodie d'un recit epique

|> Champ lexical de la guerre

|> Morale: rester humble malgre sa victoire

|> "Et voila la guerre allumee" + Antithese paix/guerre + octosyllabe --> Immediatete de la guerre

|> Vautour --> punition divine et ridiculise le coq et son orgueil

|> Parallelle christianisme/peche capital de l'orgueil?

    Plan:
    Introduction:
        Presentation generale de quelques fables de la Fontaine
        Informations generales/fonctions de la fable

        Idees de methode avant le commentaire: Choix des animaux, caracteristiques des fables, fonction des fables

        Problematique: Nous essaierons de montrer en quoi cette fable burlesque, mettant en scene l'affrontement de deux coqs a propos d'une poule, sert d'example a une morale qui prone l'humilite

    Plan:
        I. Recit en vers                    // Meh
        II. Caractere burlesque
            A) Coqs --> Animaux communs, symboles d'une volonte de seduction exageree
            B) Comparaison avec la guerre de Troie
        III. Importance de la morale
            A) Un recit construit sur l'antithese et le retournement
            B) Morale qui illustre un retournement de situation: appel a l'humilite

