# Pensez-vous que la poésie soit un moyen efficace pour manifester son engagement auprès d'une cause ?

## Mots cles

|> "poesie", "efficace", "manifester son engagement" **, "cause"**

## Reformulation

|> La poesie a-t-elle une utilite pour montrer qu'ou supporte une cause?

## Plan

### Partie 1: La poesie est un moyen efficace

|> Exemple de la poesie engagee 2e guerre mondiale

|> Textes humanistes guerres de religion

|> Victor Hugo & *Les Miserables*/*Melancolia*

### Partie 2: Contre argument

|> Peu de popularite de la poesie (maintenant)

|> Ne touche pas toute la population (ex. illetres) (historiquement)

|> Peut etre vu comme moins utile qu'une participation active
