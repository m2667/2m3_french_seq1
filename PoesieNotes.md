# **Prise de notes: La poesie**

## **L'histoire**

    > Pendant l'Antiquite, la poesie etait attribuee a une inspiration divine, elle etait utilisee pour decrire les aventures des heros mythologiques.

    > Au Moyen-Age, les chansons de gestes se developpent (utilisees pour decrire les aventures d'heros) , puis la poesie courtoise(faite por impressioner et seduire. La poesie n'est ecrite que par les nobles bien eduques).

    > Au XVIe siecle, la poesie se developpe beaucoup notamment a cause de la Pleiade. On l'utilise surtout pour amener a reflechir sur le temps qui passe, occasionellement pour critiquer la monarchie avec l'humanisme.

    > Au XVIIe siecle, la poesie devient plus definie. La poesie est desormais vue comme un art subtil, complique et travaille.

    > Au XIXe siecle, la poesie commence a perdre ses contraintes strictes(apparition de la prose) et commence a etre plus utilisee pour la critique sociale et le romantisme

    > Au XXe siecle, la poesie se diversifie bcp, les vers libres et calligrammes apparaissent, la poesie commence a decrire la vie ordinaire aussi bien que le surrealisme et la liberte de creation

## **Fonctions de la poesie**

    > Pour exprimer 
        > Emotions, sentiments, sensations
        > Nostalgie, amour, desespoir, passage du temps, amitie...
        > Fonction la plus courante
        > Lyrique => exprime des sentiments personnels.

    > Pour denoncer ou celebrer
        > Poesie engagee
        > Defend ou critique religion, politique morale
        > Victor Hugo
        > Pour raconter (Antiquite/Moyen Age)
        > Surtout XXe siecle
        > Celebration de la femme aimee, nature, Dieu
    
    > Pour Reveler
        > "Dechiffrer" le monde
        > XIXe siecle poetes comme "maudits, sotes de comprehension exceptionelle, incompris, voyants, elsu de Dieu"
        > Poetes maudits

    > Pour inventer
        > "Artisants des mots"
        > Forme tres travaillee
        > Mouvement Parnasse
        > Fonction principale => beaute
        > Calligrammes
        > Surrealistes "La Terre est bleue comme une orange"
        > OuLiPo => XXe siecle, jouent bcp avec le language

## **Formes poetiques**

    > Rondeau => 13 vers de 8/10 syllabes en 3 strophes

    > Ballade => 3 couplets + 1 demi-strophe, chaque strophe se finit par un meme refrain

    > Ode => Poeme celebrant qqch/qqn

    > Sonnet => 2 strophes de 4 vers + 2 strophes de 3 vers, rimes embrassees M/F

    > Prose => Poeme sans rimes ou vers definis

    > Calligramme => les mots dessinent le sujet
