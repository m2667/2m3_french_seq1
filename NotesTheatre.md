# Notes sur le theatre

## **INDEXE**

1. [Notes a partir de L'Anthologie](#notes-a-partir-de-L'Anthologie)

    1. [Histoire](#histoire)

    2. [Fonction](#fonction)

2. [Tragedie vs Comedie](#tragedie-vs-comedie)

3. [Tragedie](#tragedie)

4. [Le classicisme](#le-classicisme)

5. [Le baroque](#le-baroque)

    1. [Caracteristiques generales](#caracteristiques-generales)

    2. [Contexte historique](#contexte-historique)

6. [Structure de l'illusion comique](#structure-de-lillusion-comique)

7. [La scene d'exposition](#la-scene-d'exposition)

    1. [General](#general)

    2. [Dans l'illusion comique](#dans-l'illusion-comique)

8. [Actes II et III](#actes-II-et-III)

    1. [Resume](#resume)

    2. [Personnages](#personnages)

    3. [Monologue de Matamore](#monologue-de-matamore)

9. [Acte IV](#acte-iv)

    1. [Dialogue Lyse-Isabelle](#dialogue-lyse-isabelle)

## Notes a partir de L'Anthologie

### **Histoire**

|> Nait au Vie siecle av.JC avec les dionysies, traite des evenements historiques

>-> Dionysies --> fetes dediees a dionysos, dieu du vin mais aussi du theatre

|> Moyen Age --> double visage entre pieces religieuses et pieces profanes/critiquant la societe

|> XVII siecle --> naissance du baroque (cf. [Le baroque](#le-baroque))

|> Contrepartie du baroque --> le clacissisme (age d'or de la comedie)

|> XIX siecle rompt avec le classicisme --> drame romantique

|> XX siecle traite de la violence et absurdite de la condition humaine

### **Fonction**

|> XVII siecle --> lieu social, on y va pour voir & etre vu

|> XIX siecle --> combats esthetiques & politiques

|> XX siecle --> impression plutot que illusion du vrai

|> XXI siecle --> privilegie les impressions du spectateur

## Tragedie vs comedie

| Tragedie                             | Comedie               |
|--------------------------------------|:---------------------:|
| Fin emotionnelle, souvent triste     | Focalisee sur le rire |
| Aventure+personnages extraordinaires | Vie de tous les jours |
| Fatalite/mort                        |Realisme & fin heureuse|
| Langue soutenue, alexandrins    | Langue souteue ou familiere|
| 5 actes                              | 3-5 actes             |
| Titre:Nom propre                     | Titre:nom commun ou personnage collectif |

## Tragedie

|> 3 unites: une journee, un lieu, une intrigue

|> Dilemme amour/devoir

|> Fin triste/tragique

|> Personnages royaux, nobles, voire mythologiques

|> Epoque ancienne, voire mythique

|> Theme du destin

|> Alexandrins

|> Intrigue realiste et catharsis a la fin

## Le classicisme

|> Mouvement culturel et artistique surtout Francais

|> XVII-XVIII siecle

|> Retour a l'Antiquite

|> Caracteristiques: language "pur", imitation des oeuvres antiques et liens a la mythologie

|> Oeuvres qui amenent a la reflexion, humanistes

|> Auteurs connus: Corneille, de la Fontaine, Pascal ou Moliere

|> "Oppose du Baroque"

|> Regle des trois unites, cinq actes, vers alexandrins, bienseances et vraissemblance

### **Notes faites en classe**

|> 1660 - 1685

### Contexte historique

|> Commence avec le regne de L XIV, finit avec la revocation de l'edit de Nantes

|> Apogee de la Monarchie absolue: Le roi a tous les pouvoirs en supprimant le ministeriat

|> Ministres ==> deviennent executeurs de la volonte du roi

|> "L'etat c'est moi", on arrive a la monarchie de droit divin

|> Noblesse est "domestiquee" a Versailles pour mieux les controller

|> Religion: Catholicisme t. strict, protestants pas acceptes (edit de Nantes)

|> Politique de prestige: Les arts sont epanouis avec L XIV qui s'entoure d'artistes et d'ecrivains (Moliere, Bossuet, etc.)

|> France => Quasiment pays le plus important de l'epoque

### Ideaux du classicisme

|> Poete ecrit *L'art poetique* => Definit les regles du classicisme

|> Imitations des anciens (greco romains)

|> "Croyance en une nature humaine intangible, independante"

>-> Les hommes ne changent pas, comportements et moralites independantes des temps et lieux

|> Soumission aux normes => regles a respecter (ex. regle des 3 unites)

|> Respect des bienseances (pas de sang ni de violence explicite)

|> Precision, harmonie

|> Finalite moralisante => Pieces se finissent avec une morale

|> "Honnete homme" => Homme parfait
>-> Sait soutenir une conversation, etre de raison, homme de culture, elegance des manieres, ne remet pas en cause la religion & politique

### Types d'oeuvres

|> Poesie presque inexistante => Surtout pieces de theatres et livres

>-> Remplacee par les fables

|> Roman psychologique => court mais realiste et analyse psychologique t. travaillee

|> Litterature religieuse

|> Reflexion morale avec maximes ex. *Les Pensees* de B. Pascal

|> Theatre classique: "Siecle du theatre" avec Moliere, Corneille et Racine

## Le baroque

### **Caracteristiques generales**

|> 1e moitie du XVII siecle

|> Themes: illusion, reve, changement, metamorphose, chaos

|> Bcp de metaphores, hyperboles, antitheses, descriptions riches

|> Melange des genres

### **Contexte historique**

|> Reforme et contre-reforme

|> Eglise catholique invente le baroque

|> Temple protestants --> simple, austere

|> Art baroque --> Riche, emouvant

|> Modele heliocentrique --> monde bouleverse et illusion de changement & d'incertitude

## Structure de *L'illusion comique*

|> Actes 1-2 sont comiques et 3-5 sont tragiques

|> 3 niveaux:

    Niveau 1:
        |> Dialogue d'Alcandre et de Pridamant
    Niveau 2:
        |- Evocation magique du passe de Clindor
        |- Actes 2-4 (sauf scenes du niveau 3)
        |> Plus grandepart du spectacle
    Niveau 3:
        |- Clindor, Isablee et Lyse sont acteurs professionnels
        |- Jouent une tragedie
        |> Ne sont pas eux-memes mais des personnages

## La scene d'exposition

### **General**

|> Donne les personnages principaux, les enjeux, le probleme, le lieu... Tout par la parole

>-> Dans un roman le narrateur dit ca directement

|> 2 objectifs: Informer et engager

|> Repond aux 5W

|> Ne peut pas donner trop d'informations car piece devient enuuyeuse

|> Plongee au coeur d'une histoire qui a deja commence

### **Dans *L'illusion comique***

|> Ecrit en 1635 -> Mouvement baroque
>-> Longueurs et sujets diversifies et respecte peu le theatre classiqe

#### **A1S1**

|> Exemple de dissertation:

    I) Portrait de Pridamant
        1. Tourments et malheur d'un pere
            |- Pridamant a chasse son fils et le regrette
            |- On voit son amour pour son fils
            |> Les vers sont en alexandrins comme dans une tragedie

        2. Scepticisme de Pridamant
            |- Ses experiences passees le poussent au scepticisme
            |- Sa recherche est restee veine
            |- Il utilise des oppositions -> partage entre scepticisme et espoir
            |> Il a deja eu recours a la magie

    II) Un magicien etonnant
        1. Une grotte terrifiante
            |- Vit dans un lieu terrifiant
            |- Atmosphere mysterieuse
            |> Rien n'y est reel

        2. Un homme hors du commun
            |- Plonge les autres et le spectateur dans l'illusion
            |- Il construit une illusion

## Actes II et III

### **Resume**

    |> Acte II
        |- S 1:
            |> Alcandre demande a Pridamant de ne pas intervenir
        |- S 2-9:
            |- Les amours de Clindor et d'Isabelle, egalement courtisee par Adraste et Matamore
            |> Dedaignee de Clindor, Lyse aide Adraste a se venger de lui
        |> S 10:
            |> Alcandre rassure pridamant quand a la vengeance de Lyse
    
    |> Acte III
        |- S 1-11:
            |- Isabelle refusse de ceder a son pere qui lui ordonne d'epouser Adraste
            |- Clindor avoue aimer Lyse mais lui prefere la fortune d'Isabelle
            |- Matamore surprend Isabelle et Clindor mais recule face a ses menaces
            |> Adraste arrive et est tue par Clindor
        |> S 12
            |> A nouveau, Alcandre rassure Pridamant

### **Personnages**

    |> Alcandre
        |- Magicien psychologue et moraliste
        |- Image symbolique du dramaturge
        |> Role: introduit Pridamant dans le monde de l'illusion

    |> Pridamant
        |- Un pere afflige
        |- Une image symbolique du spectateur
        |> Role: il vit ce qu'il voit et est pris par les emotions face a ce qu'il voit

    |> Clindor
        |- Au service de Matamore
        |- Fils de Pridamant et donc bourgeois
        |> Amant (aime et est aime) d'Isabelle

    |> Matamore
        |- Amoureux (aime sans etre aime) d'Isabelle
        |- Maitre de Clindor
        |- Se voit en conquereur du monde surpuissant
        |> Ridicule

    |> Adraste
        |- Amoureux d'Isabelle
        |- Noble
        |> Le pere d'Isabelle veut le marier a sa fille

    |> Isabelle
        |- Amante de Clindor
        |- Aimee de Matamore et Adraste
        |> Noble

    |> Lyse
        |- Servante d'Isabelle
        |- Amante de Clindor
        |> Intelligente et rusee

### Monologue de Matamore

|> Matamore cherche a surprendre Isabelle avec son amant

|> Montre sa vraie face (peureux, ridicule)

|> Se cache de peur d'etre apercu/attaque par des servants --> opposition avec son pretendu pouvoir

|> Se prepare a courir

|> Essaie de trouver des excuses/veut pas admettre qu'il a peur

## Acte IV

### Dialogue Lyse-Isabelle

|> Opposition entre le desespoir d'Isabelle et l'espoir de Lyse

|> Scene se passe au dernier lieu ou Isabelle a vu Clindor

|> Isabelle decouvre le plan de Lyse pour sauver Clindor

|> Isabelle dit qu'elle "accompagnerait [Clindor] jusque dans les enfers"

|> Lyse raconte qu'elle a seduit le geolier pour delivrer Clindor

|> Elles prennent l'argent du pere de Lyse et s'enfuient
