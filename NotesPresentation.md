# Notes sur les presentation sur les textes humanistes

## **L'Amerique (Montaigne extrait 2)**

### *En quoi la peinture de la civilisation americaine critique la colonisation?*

    > Aussi intelligents que les Europeens
    > Sont "nus", il ne connaissent pas les avancements scientifiques de l'Europe
    > Vue comme un enfant (Champ lexical)
    > Prendra la place de l'Europe quand celle-la tombera
    > Le nouveau monde est "fouette et soumis"
    > Les amerindiens ont ete vaincus par leur propre bonte, richesse et naivete envers les Europeens qu'ils ont traites comme des dieux

## **Les tragiques (Aubigne)**

### *En quoi ce recit de guerre est-il une critique?*

    > Ecrit apres le massacre de la St.Barthelemy
    > Parle de la St.Barthelemy et du Louvre
    > Sonorites lourdes (alliterations en "T")
    > Champ lexical du corps
    > Eau de la Seine > Metaphore en riviere de sang
    > Bataille comme une mise en scene, un spectacle
    > Suggere protestants --> inhumains
    > Nobles sur un balcon, en hauteur comme spectateurs
    > Roi comapre a Sardanapale, connu pour ses orgies

## **Les Tragiques (Aubigne Extrait 2)**

### *???*

    > Tutoie Catherine de Medicis (= reine de France)
    > Critique de la reine

## **Les Tragiques(Aubigne Extrait 3)**

### *???*

    > 1e personne --> pour rendre plus vivant et reveiller des emotions
    > Registre pathetique "cris, mourant, affreux"
    > Innocence mise en avant --> "massacre piteux" et "petits enfants"
    > Suscite la pitie du lecteur
    > Vocabulaire violent

## **Discours des miseres de notre temps (Ronsard)**

### *Comment Ronsard critique-t-il le protestantisme?*

    > Ronsard
        > forme la Pleiade
        > "Prince des poetes"
    > Critique du protestantisme
        > Allegorie
        > Critique des humanistes protestants, "ces nouveaux rabins"
        > Protestantisme --> "misere de notre temps"
        > Opinion --> Allegorie du protestantisme
            > Fille de Jupiter et de Presemption
            > Cuider --> nourrice
            > Sirenes --> Attire et piege
        > Reprise de la mythologie: Jupiter, Mont Olympe, Mythes des sirenes et des geants...
