# Questions sur le *Discours sur la Servitude volontaire* de La Boetie

## **1. Comment definiriez-vous un traite ? (Le Discours sur la Servitude volontaire est un traité).**

    > Selon moi, un traite est un texte qui parle en profondeur d'un sujet, souvent en le critiquant.

## **2. Résumez chaque paragraphe en une ou deux phrases pour faire apparaître le raisonnement de la Boétie.**

    > 1. Les hommes sont tous soumis a un seul, il ne peuvent rien y faire car ils ne sont pas unis et en desordre.
    > 2. Les hommes par leur nature ont besoin d'une hierarchie et si quelqu'un est apte a diriger, il devrait naturellement monter au pouvoir.
    > 3. Les dirigeants du XVI siecle sont faibles (comparaison a Mirmidon), il est difficile de croire que personne ne se rebelle contre un homme qui ne pourrait rien faire pour se defendre.

## **3. Comment le paradoxe de la tyrannie est-il présenté ?**

    > Il est presente comme insense, la tyrannie comme joug facile a perdre mais auquel personne ne s'oppose.


    C> Le tyran ne doit pas son pouvoir a ses talents mais a son ascendance, il n'a pas de pouvoir autre que celui que le peuple lui attribue (un homme contre un peuple)

## **4. Comment cherche-t-il à faire réagir le lecteur ?**

    > Il veut forcer le lecteur a penser a vaincre la tyrannie, il incite a une rebellion.


    C> discours rhetorique, questions rhetoriques, phrases exclamatives pour ^^. Il amene a reflechir a la situation, veut reveiller un sentiment d'honneur, insuffle une volonte d'agir.

## **5. En quoi ce texte est-il représentatif de l’humanisme ?**

    > Son but primaire est de forcer le lecteur a raisonner, il cherche aussi a ramener le pouvoir au peuple, a faire tomber les traditions insensees du Moyen Age.

## **6. Rappelez rapidement l’opposition entre Pantagruel et Picrochole dans Gargantua.**

    > Pantagruel est presente comme roi bon, bien eduque, preferant eviter la guerre et resoudre les conflits par la diplomacie. Picrochole est son contraire: impulsif, stupide et cherchant toujours un moyen de conquerir un peu plus de terres et d'agrandir son influence.


    C> Pantagruel => roi humaniste, represente Francois Ier; Picrochole => roi de l'Ancien Regime, represente Charles Quint

## **Plan de dissertation**

    C> I. Le paradoxe de la tyrannie
        |-> disproportion des forces
        --> mediocrite du tyran

       II. Un discours persuasif
        |-> le peuple comme destinataire
        |-> discours construit pour entrainer l'adhesion
        --> critique violente pour souligner l'indignation

        III. Un discours novateur
        |-> refus du religieux
        |-> remise en cause d'un systeme politique dominant
        --> confiance en l'homme (optimisme dans la nature humaine et sa capacite a reagir)
