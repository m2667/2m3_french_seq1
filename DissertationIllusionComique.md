# Travail sur le sujet sur l' *Illusion comique*

Sujet :"Il s'agit de peindre un monde instable, en proie au changement, en perpétuelle représentation, un monde de violence extrême, mais aussi de raffinement."

Cette définition du baroque s'applique-t-elle à la pièce de Corneille L'Illusion comique ?

Introduction: Definir le baroque, problematique, plan

I) Contexte historique

- Decouvertes de Copernic
- Reforme / Contre-Reforme
- Characteristiques du baroque

II) Pour selon l'*Illusion Comique*

- Niveaux de lecture
- Mort de Clindor
- Chaos et confusion general
