# Dissertation sur *Andromaque* de Racine

## "Ce n'est point une necessite qu'il y ait du sang et des morts dans une tragedie il suffit que l'action en soit grande, que les acteurs en soient heroiques, que les passions y soient excitees et que tout s'y ressente de cette tristesse majestueuse qui fait tout le plaisir de la tragedie"

### Plan (maison)

    |- Axe I: La violence
         |- Violence interdite (bienseance et regles du classicisme)
         |- Violence hors de la scene (Mort de Pyrrhus & Hermione)
    |- Axe II: Personnages
         |- Heroiques (Rois/nobles, etc.)
         |- Passions (Amour, dechirement devoir/sentiment)
    |- Axe III: "Tristesse majestueuse"
         |- Catharsis
         |- But du classicisme: Etude de la "nature intangible de l'Homme", Etude des moralites face aux choix difficiles

### Travail sur l'enonce

|> Acteurs: arttiste dont la profession est de jouer un role/Personne qui prend une part active ou un role important

|> Heroique: dignes d'un heros

|> Passions: etat affectif ou mental dominant la vie mentale

|> Excitees: anormalement vives

|> Majestueuse: d'une beatue pleine de grandeur, de noblesse

|> Theme -> la tragedie

|> Reformulation: Racine veut dire en un premier temps qu'il n'y a pas besoin de montrer violence dans une tragedie. Il suffit simplement d'une intrigue extraordinaire, que les personnages soient braves et leurs sentiments pousses a l'extreme

### Plan Alternatif

     |- Axe I: Actions grandes/personnages Heroiques
          |- Sujets serieux (pouvoir, honneur)
          |- Personnages = heros
     |- Axe II: Passions excitees
          |- Amour extreme 
          |- Jalousie devorante
     |- Axe III: Tristesse majestueuse
          |- Acceptation du sacrifice
          |- Fatalite/mort

### Introduction

La tragedie est un genre qui a souvent des themes touchant a la mort,le sacrifice, etc., mais est-ce vraiment necessaire? La violence
 et la mort doivent-elle apparaitre, ou est-ce que la tragedie peut etre definie par d'autres characteristiques? Nous allons voir
 ceci en trois parties: Tout d'abord les personnages eux-memes, ainsi que leurs actions et les enjeux de celles-ci, puis on va passer
 a leurs emotions, amour ou jalousie plus extremes qu'elles ne peuvent l'etre reellement, en finissant sur la "Tristesse majestueuse"
 mentionnee dans l'enonce, et comment elle semble requerir la mort et la violence.
