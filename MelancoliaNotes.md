# Analyse de *Melancolia*

## Relever et expliquer 5 procedes litteraires

    > "doux etres pensifs que la fievre maigrit" => opposition entre l'innocence et la beaute des enfants et le travail et la fatigue qui les ravagent.

    > "dents d'une machine sombre", "monstre hideux qui mache" => personnification de la machine, elle est vue comme un monstre.

    > "O Dieu!" => apostrophe, V.Hugo appelle Dieu pour demander la liberation des enfants du travail.

    > "Maudit comme le vice ou l'on s'abatardit,/Maudit comme l'oppobre et comme le blaspheme!" => anaphore, accentuant la nature mauvaise du travail des enfants.

    > "Ou vont tous ces enfants dont pas un seul ne rit ?/ Ces doux etres pensifs que la fievre maigrit ?" => Questions rhetoriques, montrent un doute, l'auteur "ne comprend pas" ce qu'ils font.
