# Dissertation sur la Princesse de Cleves

## La princesse de Clèves symbolise bien la lutte violente, épuisante, d'une femme qui se libère des entraves de la société masculine et acquiert son autonomie

### Plan

*La Princesse de Cleves* montre-t-elle une forme de proto-feminisme?

I. Mouvement precieux
a/ Tendance vers l'education et maniere de vivre riche des femmes
b/ Rejet du mariage, amants nombreux

II. La Princesse est-elle precieuse?
a/ Oui (independante de son mari, courtise m. de Nemours)
b/ Non (mariee, demande pardon a son mari)

III. Une societe masculine
a/ Critique de la preciosite
b/ Societe masculine (?)


### Dissertation

*La Princesse de Cleves* est un roman precieux publie par Madame de La Fayette en 1678. Aujourd'hui, on peut se demander si la *Princesse* montre une certaine forme de proto-feminisme,
et ce sera a cette question a laquelle je tenterai de repondre aujourd'hui. On va commencer par etudier le monvement precieux, duquel Madame de la Fayette fait partie, afin de voir si
ce mouvement en soi-meme peut etre une forme de feminisme. On va continuer par analiser le personnage de la Princesse, et si elle ferait partie de ce mouvement. Enfin, on va analiser la
societe du temps, et l'inegalite des sexes du temps.

La principale caracteristique du mouvement precieux est le style de parole et d'ecriture extremment riche, presque ridicule. Malgre la connotation negative que cela pourrait avoir,
ceci indiquerait que les femmes commencent a s'eduquer, et regagner une certaine place plus importante dans la societe, ce qui montrerait une certaine forme d'avancement dans les droits
des femmes.

L'autre caracteristique precieuse est le rejet du mariage, la volonte de se rendre inaccessible et de n'avoir que des amants, sans la contrainte du mariage. Pourquoi le mariage est il vu
comme une contrainte au XVII siecle? En effet, par le mariage, une femme serait, du point de vue de la societe, entierement soumise a son mari, jusqu'a la mort de ce dernier. Cela
montrerait donc, une fois de plus, que les precieuses tentent de garder et faire croitre leur droits et libertes, ce qui concluerait que leur mouvement peut-etre considere un precurseur
du feminisme.

Maintenant, etudions la princesse elle-meme. En effet, il est difficile de justifier qu'elle soit ou pas precieuse. D'abord, la Princesse semblerait faire partie du mouvement precieux:
elle est independente de son mari, parle dans un style plutot riche, et est amoureuse de M.de Nemours. Tout semblerait-il donc pointer vers la princesse etant une precieuse? Non, en effet, 
il semblerait qu'il existerait plus d'arguments contraires a un tel raisonnement.

De l'autre cote, la princesse est quand-meme mariee, et dans la scene de l'aveu, elle se soumet a M. de Cleves, ce qui est directement contraire au portrait de la preciosite etabli plus
haut. Son style de parole lui aussi, quoique riche, n'est pas surdecore comme celui des precieuses, et pourrait simplement etre attribue a la haute naissance de la Princesse. De plus, 
quoique amoureuse de M. de Nemours, la princesse ne trompe jamais a vrai dire son mari et essaye de garder une distance entre elle et le duc. En effet, a la fin du roman, la princesse
decide de rejeter M. de Nemours et se retirer loin de la cour suite a la mort de son mari, ce qui est aussi extremmement contraire au style de vie precieux, consistant a etre au centre
de l'attention et d'etre toujours courtisee par des hommes, sans jamais se marier.

Enfin, nous allons etudier la societe autour de la princesse. En effet, Beaucoup d'auteurs hommes ont critique les precieuses, surtout Moliere, avec *les Precieuses ridicules* et
*les Femmes savantes*. Dans ces pieces, Moliere satirise les tentations des femmes a prendre de l'importance et s'eduquer, ainsi que le mouvement precieux et ses imitatrices. En effet,
les femmes precieuses des hautes spheres sociales ont inspire des femmes de rang social plus bas a tenter de devenir precieuses a leur tour, sans y parvenir vraiment.

Pour finir, il est important de noter le rapport des genres a la societe du XVII siecle en general: Jusqu'au XXe siecle, la place stereotypee de la femme est au foyer, a faire des travaux
manuels, s'occuper des enfants, etc. et non pas dans la politique, l'armee, et mal vue dans l'ecriture, la philosophie et la science. Il est donc naturellement mal vu que un mouvement
encourageant l'independance, l'education et la culture feminine serait mal vu au XVIIe siecle.

Pour repondre donc a la problematique: Autant que le mouvement precieux symbolise, voire est une lutte contre une societe masculine relativement hostile, il est debatable si le personnage
de la Princesse serait reellement precieux, et mon point de vue personnel est que non, *La princesse de Cleves* n'est pas reellement une forme de proto-feminisme, ou precurseur d'un tel
mouvement.

