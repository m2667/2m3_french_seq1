# Commentaire de texte sur la princesse de cleves

## Personnages

- Princesse de Cleves
- Duc de Nemours
- Mme. la Dauphine
- M. de Cleves

### Princesse de Cleves

|> Peu d'indications sur le physique mais blonde, belle et pale

|> Mariee a M. de Cleves

|> Aime le duc de Nemours

|> Laisse le duc voler son portrait

### Duc de Nemours

|> Aime la Princesse de Cleves

|> Beau, provient d'une famille puissante

|> Vole le portrait et demande a la Princesse de ne pas le devoiler

### Mme. la Dauphine

|> Femme du Dauphin; Dauphin -> Futur roi

|> Personnage secondaire, n'apercoit pas le vol

### M. de Cleves

|> Marie a la Princesse de Cleves

|> Ne soupconne rien serieusement

## Plan

        Plan de dissertation:
            I) Beaute de la Princesse
                A. Description initiale dans le roman
                B.
            II) Comportement de M. et Mme. de Cleves
                A. M. de Cleves ne soupçonne rien
                B. Mme. de Cleves laisse faire M. de Nemours
            III) Comportement de M. de Nemours
                A. Embarras face a Mme de Cleves
                B. Joie d'avoir le portrait
