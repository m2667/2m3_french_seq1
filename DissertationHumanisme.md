# Dissertation sur l'humanisme

## Recherche d'idees et introduction

L'humanisme est souvent defini comme un retour a l'Antiquite, mais c'est aussi le point d'ou est partie la plupart de la science et de l'art qu'on cnnait aujourd'hui. *Comment l'humanisme de la Renaissance a-t-il réussi à concilier deux aspirations apparemment contradictoires : le retour à l'Antiquité et une vision nouvelle et moderne du monde et de l'Homme ?* C'est-a-dire, **comment arrive-t-il a joindre l'Antiquite et sa vision moderne du monde?** Nous allons donc etudier comment l'humanisme relie-t l'Antiquite et la science moderne. On va commencer par voir comment lAntiquite est incorporee, avec la reprise des textes antiques et l'etude du corps humain, puis on va s'interesser au monde moderne: comment la connaissance de l'Homme et du monde a explose, puis comment des nouvelles techniques en art et sience se sont developees.
