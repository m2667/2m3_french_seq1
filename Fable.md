# Travail d'ecriture d'une fable

## Le pivert et le renard

Autrefois, sous un chene, maitre Renard logea.

Un Pivert arrivant, commode l'arbre trouva,

Et plein de scarabees, vers et toute vermine.

Sans attendre, il se mit a son occupation,

Retentit le bruit faisant sa reputation.

Renard qui dans sa taniere calmement sommeillait

Brusquement par le chahut devint vite eveille,

Et le Pivert commenca a ainsi conjurer:

"Mon tres cher, arrete donc ton affreux cliquetage,

Malgre son but noble, il me deplait affreusement."

Renard argumenta pour le demenagement,

"N'y a-t-il point d'autres arbres dans la region?"

Pivert, loin du danger, n'y paya pas attention.

Sans le moindre souci, continuant son action.

La nuit venue, Renard au nid du Pivert arrive,

Et en vengeant le repos de sa journee perdue,

Il fit de sorte que l'oiseau ne se reveilla plus.

Ainsi on en tire une morale pour notre monde:

On refuse pas a un puissant guere plus d'une fois,
