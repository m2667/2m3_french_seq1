# Travail par groupes de 2-3 sur les textes humanistes

## Eloge de la folie [1]

    > Met en avant les abus, le materialisme de l'Eglise
    > Le spirituel est au deuxieme plan
    > Absurdite de calculer le prix des fautes et le temps au purgatoire
    > Critique des indulgences
    > Les hommes se promettent le paradis malgre leur crimes
    > Allegorie de la Folie dit "folies folles" en se referant a ^
    > Attribution de trop de valeurs aux saints selon l'auteur

## Eloge de la folie [2]

    > Princes distraits par plaisirs materiaux
    > Princes --> Exemples pour le peuple
    > Bcp d'enumerations pour accentuer la negligence
    > Ne pensent qu'a prendre l'argent du peuple et le mettre dans leurs poches
    > Prince compare a personnage de teatre avec attributs royaux comme deguisement
    > Princes indignes de leurs insignes

## Le Prince

    > Roi doit etre dur ou flexible selon les conditions
    > Comparation au lion et au renard
    > Rois mauvais
    > Rois trompent les hommes

## Discours sur la servitude volontaire

    > 
